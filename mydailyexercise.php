<?php require('requiredfile.php'); ?>
<!DOCTYPE html>
<html lang="en">
        <title>Nutritional Intake</title>
        <link rel="icon" href="icon.jpg" type="image/jpg" sizes="20x20">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
            .sidenav {
            height: 100%;
            width: 500px;
            position: absolute;
            z-index: 1;
            top: 0;
            left: 20px;
            overflow-x: hidden;
            padding-top: 300px;
            }
            .sidenav a {
                padding: 6px 8px 6px 16px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
            }
  
            .sidenav a:hover {
                color: rgba(173,199,67,1);
            }
  
            .main {
                border-top-style: solid;
                border-top-color: #384119;
                margin-left: 160px; /* Same as the width of the sidenav */
                font-size: 28px; /* Increased text to enable scrolling */
                padding-left:40%;
                padding-bottom:30px;
            }
            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }
            body{
                font: 15px/1.5 Times, Helvetica, sans-serif;
                padding:0;
                margin:0;
                margin-top:60px;
                background-color:white; 
                background-attachment: fixed;  
            }
            
            .head{
                background-color:rgba(173,199,67,1);
                width:15%;
                text-align:center;
                margin-left:10%;
                margin-bottom:0.5%;
            }
            .head p{
                font-family:serif;
                font-size:20px;
                color:#555353;
                margin-top:0%;
            }
            .socmed{
                padding-left:63%;
            }
            h2{
                color:#ffffff;
                font-size:30px;
                font-family:serif;
            }
            h3{
                color:#384119;
                font-family:serif;
                font-size:50px;
                font-style:bold;
            }
            ul a{
                color: #384119;
                text-decoration:none;
                font-size: 20px;
                font-weight: bold;
                text-align:center;
                font-family:arial;
            }
            header li{
                float:left;
                display:inline;
                padding: 0 20px 0 20px;
            }
            header nav{
                float:left;
                margin-top:5px;
                margin-left:25%;
            }
            header a:hover{
                color: #CCE472;
            }
            #showcase{
                min-height: 600px;
                background:url(https://images.pexels.com/photos/41660/apple-bite-diet-eat-41660.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940)no-repeat 0 -400px;
                text-align:center;
                font-weight: bold;
                background-size:100%;
                background-attachment: fixed;
                color:#384119;
            }
            #showcase h1{
                margin-top:100px;
                font-size:55px;
                margin-bottom:10px;
            }
            p{
                font-family:arial;
                font-style:oblique;
                font-size:20px;
                padding:0;
            }
            .box{
                padding-left:9%; 
                border-style: groove;            
            }
            .box h3{
                padding-top:20px;
                width:600px;
                color:#384119;
                font-family:arial;
            }
            .box h4{
                padding-top:20px;
                width:600px;
                color:#384119;
                font-family:arial;
                font-size:30px;    
            }
            .box ul{
                font-size:20px;
                font-family:arial;
                color:#384119;                
            }
            .box a:hover{
                color: #CCE472;
            }
            .container a{
                text-decoration:none;
                font-family:serif;
                font-style:bold;
                font-size:25px;
                color:#384119;
            }
            footer{
                margin-top:5%;
                background-color: rgba(173,199,67,1);
                height:100px;
                font-size:20px;
                text-align:center;
                padding-top:30px;
                color:white;
            }
            .box a{
                font-size:20px;
                color:#384119;
                font-style:arial;
            }
            .main p{
                font-family:serif;
                font-size:20px;
                margin-right:20px;
            }
            .now a{
                color:rgba(173,199,67,1);
            }
            .main h1{
                font-family: Georgia, 'Times New Roman', Times, serif;
                font-style:oblique;
                font-size:50px;
            }

        </style>
    <body>
        <header>
            <nav>
                <ul>
                    <li class="current"><a href="index.html">Home</a></li>
                    <li><a href="aboutus.html">About Us</a></li>
                    <li><a href="contactus.html">Contact Us</a></li>
                </ul>
                <ul>
                    <li class="socmed"><a href="https://www.facebook.com" class="fa fa-facebook"></a></li>
                    <li><a href="https://www.twitter.com" class="fa fa-twitter"></a></li>
                    <li><a href="https://www.instagram.com" class="fa fa-instagram"></a></li>
                </ul>
            </nav>    
            <div class="head">
                    <h2>NUTRITIONAL<br>INTAKE</h2>
                    <p>A Health Website</p>   
            </div>
        </header>
        <section id="boxes">
            <div class="container">
                <div class="sidenav">
                    <ul style="list-style-type:none">
                        <?php
                            if ($_SESSION['username']!="") {
                                echo "<li class=\"now\"><a href=\"myaccount.php\" style=\"color:gray;font-style:oblique\">".$_SESSION['username']."</a></li>";
                            }
                            else echo "<li><a href=\"login.php\">My Account</a></li>";
                        ?>
                        <li><a href="mydailyconsume.php">My Daily Consume</a></li>
                        <li class="now"><a href="#">My Daily Exercise</a></li>
                    </ul>        
                </div>
                <div class="main">
                    <h1>My Daily Exercise</h1>
                    <image src="dailyexercise.jpg" style="width:600px;height:1000px"></a></image>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">I Have Done It!</button>
                </div>
                
            </div>
        </section>  
        <footer>
            <i class="fa fa-copyright">2019 by Etasa K.</i>
        </footer>
  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Notification</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          You have done it, keep it up!
        </div>
      </div>
    </div>
   </div>
    </body>
</html>
