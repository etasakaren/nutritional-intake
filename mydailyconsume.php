<?php require('requiredfile.php'); ?>
<!DOCTYPE html>
<html lang="en">
        <title>Nutritional Intake</title>
        <link rel="icon" href="icon.jpg" type="image/jpg" sizes="20x20">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
            .sidenav {
            height: 100%;
            width: 500px;
            position: absolute;
            z-index: 1;
            top: 0;
            left: 20px;
            overflow-x: hidden;
            padding-top: 300px;
            }
            .sidenav a {
                padding: 6px 8px 6px 16px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
            }
  
            .sidenav a:hover {
                color: rgba(173,199,67,1);
            }
  
            .main {
                border-top-style: solid;
                border-top-color: #384119;
                margin-left: 160px; /* Same as the width of the sidenav */
                font-size: 28px; /* Increased text to enable scrolling */
                padding-left:40%;
                padding-bottom:30px;
            }
            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }
            body{
                font: 15px/1.5 Times, Helvetica, sans-serif;
                padding:0;
                margin:0;
                margin-top:60px;
                background-color:white; 
                background-attachment: fixed;  
            }
            
            .head{
                background-color:rgba(173,199,67,1);
                width:15%;
                text-align:center;
                margin-left:10%;
                margin-bottom:0.5%;
            }
            .head p{
                font-family:serif;
                font-size:20px;
                color:#555353;
                margin-top:0%;
            }
            .socmed{
                padding-left:63%;
            }
            h2{
                color:#ffffff;
                font-size:30px;
                font-family:serif;
            }
            h3{
                color:#384119;
                font-family:serif;
                font-size:50px;
                font-style:bold;
            }
            ul a{
                color: #384119;
                text-decoration:none;
                font-size: 20px;
                font-weight: bold;
                text-align:center;
                font-family:arial;
            }
            header li{
                float:left;
                display:inline;
                padding: 0 20px 0 20px;
            }
            header nav{
                float:left;
                margin-top:5px;
                margin-left:25%;
            }
            header a:hover{
                color: #CCE472;
            }
            p{
                font-family:arial;
                font-style:oblique;
                font-size:20px;
                padding:0;
            }
            .box{
                padding-left:9%; 
                border-style: groove;            
            }
            .box h3{
                padding-top:20px;
                width:600px;
                color:#384119;
                font-family:arial;
            }
            .box h4{
                padding-top:20px;
                width:600px;
                color:#384119;
                font-family:arial;
                font-size:30px;    
            }
            .box ul{
                font-size:20px;
                font-family:arial;
                color:#384119;                
            }
            .box a:hover{
                color: #CCE472;
            }
            .container a{
                text-decoration:none;
                font-family:serif;
                font-style:bold;
                font-size:25px;
                color:#384119;
            }
            footer{
                margin-top:5%;
                background-color: rgba(173,199,67,1);
                height:100px;
                font-size:20px;
                text-align:center;
                padding-top:30px;
                color:white;
            }
            .box a{
                font-size:20px;
                color:#384119;
                font-style:arial;
            }
            .now a{
                color:rgba(173,199,67,1);
            }
            .main h1{
                font-family:Georgia, 'Times New Roman', Times, serif;
                font-size:40px;
                font-style:oblique;
                margin-bottom:30px;
            }
            td{
                text-align: center;
            }
            th{
                background-color:rgb(184, 206, 94);
            }
            table, th, td {
                 border: 1px solid black;
                 font-family:arial;
                 margin-left:10px;
                 margin-right:10px;
                 margin-top:10px;
                 margin-bottom:10px;
                 font-size:20px;
            }
            .scale p{
                font-family:serif;
                opacity:0.5;
                font-style:oblique;
            }
            h4{
                font-family:serif;
                font-style:oblique;
                opacity:0.7;
            }
        </style>
    <body>
        <header>
            <nav>
                <ul>
                    <li class="current"><a href="index.html">Home</a></li>
                    <li><a href="aboutus.html">About Us</a></li>
                    <li><a href="contactus.html">Contact Us</a></li>
                </ul>
                <ul>
                    <li class="socmed"><a href="https://www.facebook.com" class="fa fa-facebook"></a></li>
                    <li><a href="https://www.twitter.com" class="fa fa-twitter"></a></li>
                    <li><a href="https://www.instagram.com" class="fa fa-instagram"></a></li>
                </ul>
            </nav>    
            <div class="head">
                    <h2>NUTRITIONAL<br>INTAKE</h2>
                    <p>A Health Website</p>   
            </div>
        </header>
        <section id="boxes">
            <div class="container">
                <div class="sidenav">
                    <ul style="list-style-type:none">
                        <?php
                            if ($_SESSION['username']!="") {
                                echo "<li class=\"now\"><a href=\"myaccount.php\" style=\"color:gray;font-style:oblique\">".$_SESSION['username']."</a></li>";
                            }
                            else echo "<li><a href=\"login.php\">My Account</a></li>";
                        ?>
                        <li class="now"><a href="#">My Daily Consume</a></li>
                        <li><a href="mydailyexercise.php">My Daily Exercise</a></li>
                    </ul>        
                </div>
                <div class="main">
                    <h1>My Daily Consume</h1>
                    <div id="mytable">
                           <form method="post" autocomplete="off">
                            <table style="width:100%">
                               
                                <tr>
                                      <th></th>
                                      <th>Foods</th>
                                      <th>Beverages</th> 
                                    </tr>
                                    <tr>
                                      <td>Breakfast</td>
                                      <td>
                                            <input type="text" name="foodsB" style="border:0;"><br>
                                      </td>
                                      <td>
                                            <input type="text" name="beveragesB" style="border:0;"><br>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Lunch</td>
                                        <td>
                                            <input type="text" name="foodsL" style="border:0;"><br>
                                        </td>
                                              <td>
                                            <input type="text" name="beveragesL" style="border:0;"><br>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td>Dinner</td>
                                      <td>
                                            <input type="text" name="foodsD" style="border:0;"><br>
                                      </td>
                                      <td>
                                            <input type="text" name="beveragesD" style="border:0;"><br>
                                      </td>
                                    </tr>
                                
                                   
                            </table>
                            <button class="button" name="submitCons">Submit</button></form>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#urModal">
  View what you have eaten
                    </button>
                </section>
        <footer>
            <i class="fa fa-copyright">2019 by Etasa K.</i>
        </footer>
          
          <!-- The Modal -->
    <?php
        if(isset($_SESSION['username'])){
            $helperName=$_SESSION['username'];
            echo "<div class=\"modal\" id=\"urModal\">
    <div class=\"modal-dialog\">
      <div class=\"modal-content\">
      
        <div class=\"modal-header\">
          <h4 class=\"modal-title\">Notification</h4>
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
        </div>
        
        <div class=\"modal-body\">";
        $res=mysqli_query($db,"SELECT * FROM recorder WHERE name='$helperName' ORDER BY id_coloumn DESC LIMIT 1");
        while($target=mysqli_fetch_assoc($res)){
            echo "<table style=\"width:100%\">
                               
                                <tr>
                                      <th></th>
                                      <th>Foods</th>
                                      <th>Beverages</th> 
                                    </tr>
                                    <tr>
                                      <td>Breakfast</td>
                                      <td>
                                            ".$target['food_breakfast']."<br>
                                      </td>
                                      <td>
                                            ".$target['bev_breakfast']."<br>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Lunch</td>
                                        <td>
                                            ".$target['food_lunch']."<br>
                                        </td>
                                              <td>
                                            ".$target['bev_lunch']."<br>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td>Dinner</td>
                                      <td>
                                            ".$target['food_dinner']."<br>
                                    </td>
                                      <td>
                                            ".$target['bev_dinner']."<br>
                                      </td>
                                    </tr>
                                
                                   
                            </table>";
        }
        echo "</div>
      </div>
    </div>
   </div>";      
        }
    ?>
    </body>
</html>
